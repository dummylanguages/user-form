# User Form
A user form with manual validation; just to practice **regular expressions** (It uses no JS frameworks, just vanilla JavaScript).

Click [here](https://dummylanguages.gitlab.io/user-form/index.html) to see it live!

![User Form Screenshot](user_form_screenshot.png)

## Dependencies:

* [Bootstrap 5](https://getbootstrap.com)
