const nameElem  = document.getElementById('name');
const zipElem   = document.getElementById('zip');
const emailElem = document.getElementById('email');
const phoneElem = document.getElementById('phone');

nameElem.addEventListener('blur', validateName);
zipElem.addEventListener('blur', validateZip);
emailElem.addEventListener('blur', validateEmail);
phoneElem.addEventListener('blur', validatePhone);

function validateName() {
  const name = nameElem.value;
  const regex = /^[a-zA-Z]{2,10}$/;
  if (!regex.test(name))
    nameElem.classList.add('is-invalid');
  else
    nameElem.classList.remove('is-invalid');
  console.log(name);
}

function validateZip() {
  const zip = zipElem.value;
  const regex = /^\d{5}(-\d{4})?$/;
  if (!regex.test(zip))
    zipElem.classList.add('is-invalid');
  else
    zipElem.classList.remove('is-invalid');
  console.log(zip);
}

function validateEmail() {
  const email = emailElem.value;
  const regex = /^([\w.-]+)@([\w.-]+).([a-z]{2,5})$/;
  if (!regex.test(email))
    emailElem.classList.add('is-invalid');
  else
    emailElem.classList.remove('is-invalid');
  console.log(email);
}

/*
**  Acceptable formats:
**  123-123-1234
**  123.123.1234
**  123 123 1234
** (123) 123 1234
** (123)-123-1234
**  1231231234
*/
function validatePhone() {
  const phone = phoneElem.value;
  const regex = /^\(?\d{3}\)?[-. ]?\d{3}[-. ]?\d{4}$/;
  if (!regex.test(phone))
    phoneElem.classList.add('is-invalid');
  else
    phoneElem.classList.remove('is-invalid');
  console.log(phone);
}